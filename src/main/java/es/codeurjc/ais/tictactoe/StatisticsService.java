package es.codeurjc.ais.tictactoe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class StatisticsService {
    private Map<String, Player> players = new HashMap<>();

    public Player getPlayer(String name) {
        return players.get(name);
    }

    public List<Player> getPlayers() {
        return new ArrayList<>(players.values());
    }

    /**
     * Método para añadir un jugador si no existe
     *
     * @param player Jugador
     *
     * @return 'True' si no existe; 'False' en caso contrario
     */
    public boolean addPlayer(Player player) {
        if (!this.players.containsKey(player.getName())) {
            this.players.put(player.getName(), player);
            return true;
        }
        return false;
    }
}
