package es.codeurjc.ais.tictactoe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StatisticsController {
    @Autowired
    private StatisticsService players;

    @RequestMapping(value = "/stats")
    public String estadistica(Model model) {
        model.addAttribute("listaPlayer", players.getPlayers());
        return "stats";
    }
}
